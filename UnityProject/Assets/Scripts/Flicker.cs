﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class Flicker : MonoBehaviour
{

    private Light _light;
    [SerializeField]
    private float minIntensity;
    [SerializeField]
    private float maxIntensity;

    void Start()
    {
        _light = GetComponent<Light>();
    }

    void Update()
    {
        _light.intensity = Mathf.Lerp(_light.intensity, Random.Range(minIntensity, maxIntensity), Time.deltaTime * 10);
    }
}
