﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
public class Wave : NetworkBehaviour
{

    public float speed = 10;
    public float force = 100;
    private Rigidbody rigid;

    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        rigid.useGravity = false;
        Destroy(gameObject, 1);
    }

    void OnDestroy()
    {
        ResourceManager.InstantiateRockBang(transform.position);
    }

    void OnTriggerEnter(Collider col)
    {
        Rigidbody rb = col.GetComponent<Rigidbody>();
        if (rb != null)
        {
            Player p = col.GetComponent<Player>();
            if (p != null)
                Push(p);

            rb.AddForce(rigid.velocity.normalized * force);
        }
    }

    void Push(Player player)
    {
        if (isServer)
            player.CmdDropPickup(-rigid.velocity.normalized + player.transform.position, -rigid.velocity.normalized * 100);
    }

    void Update()
    {
        rigid.velocity = transform.forward * speed;
    }
}
