﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


[RequireComponent(typeof(Rigidbody))]
public class Pickup : NetworkBehaviour
{

    public GameObject desiredPos;
    private Rigidbody rigid;

    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        if (!isServer)
        {
            GetComponent<Collider>().enabled = false;
            rigid.useGravity = false;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (!isServer) return;
        Player p = col.gameObject.GetComponent<Player>();
        if (p)
        {
            p.SetPickup(this);
            RpcPickedUp(true, Vector3.zero, Vector3.zero);
        }
    }

    [ClientRpc]
    public void RpcPickedUp(bool state, Vector3 pos, Vector3 force)
    {
        gameObject.SetActive(!state);
        rigid.MovePosition(pos);
        rigid.AddForce(force, ForceMode.Impulse);
    }

    void Update()
    {
        if (desiredPos != null)
        {
            rigid.MovePosition(desiredPos.transform.position);
        }
    }
}
