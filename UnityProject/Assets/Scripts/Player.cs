﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
{
    public bool canShoot = true;
    public float cooldown = 2;
    private float defaultCooldown;
    public Pickup pickup;
    private Rigidbody rigid;
    private NetworkStartPosition[] respawnPositions;
    private GameManager manager;
    private ShakeCam shaker;
    private GameObject deadCam;
    private Health health;
    private PlayerAnimationController anim;
    [SerializeField]
    private AudioListener selfCam;
    [SerializeField]
    private GameObject[] disableInNetwork;
    [SerializeField]
    private Component[] disableInNetworkComp;
    [SerializeField]
    private MeshRenderer staff;
    [SerializeField]
    private MeshRenderer orb;
    [SerializeField]
    private SpriteRenderer player;
    [SerializeField]
    private GameObject[] hearts;
    [HideInInspector]
    public bool dead = false;

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        manager = FindObjectOfType<GameManager>();
        Color pColor = manager.PlayerColor(this);
        if (pColor != Color.cyan)
            staff.material.color = player.material.color = orb.material.color = pColor;
        shaker = GetComponentInChildren<ShakeCam>();
        health = GetComponent<Health>();
        if (isLocalPlayer)
        {
            if (selfCam)
            {
                deadCam = Camera.main.gameObject;
                Camera.main.gameObject.SetActive(false);
                selfCam.enabled = true;
            }
            respawnPositions = FindObjectsOfType<NetworkStartPosition>();
            anim = GetComponent<PlayerAnimationController>();
            anim.myself = true;
            defaultCooldown = cooldown;
        }


        if (!isLocalPlayer)
        {
            foreach (GameObject g in disableInNetwork)
                g.SetActive(false);
            foreach (Component c in disableInNetworkComp)
                (c as MonoBehaviour).enabled = false;
            enabled = false;
        }
        rigid = GetComponent<Rigidbody>();
    }

    void OnDestroy()
    {
        if (deadCam)
            deadCam.SetActive(true);
    }

    [Command]
    public void CmdRespawn()
    {
        if (isServer)
            RpcRespawn();
    }

    public void UpdateHealth(int increment)
    {
        health.TakeDamage(increment);
        if (health.currentHealth <= 0)
        {
            dead = true;
            NetworkServer.Destroy(gameObject);
        }
    }

    [ClientRpc]
    private void RpcRespawn()
    {
        if (pickup)
        {
            Destroy(pickup.gameObject);
            manager.CmdRespawnPickup();
            SetPickup(null);
        }
        if (!isLocalPlayer) return;

        transform.position = respawnPositions[Random.Range(0, respawnPositions.Length)].transform.position;
        canShoot = true;
    }

    [Command]
    public void CmdDropPickup(Vector3 wavePos, Vector3 force)
    {
        if (pickup)
        {
            pickup.RpcPickedUp(false, wavePos, force);
            SetPickup(null);
        }
    }

    [Command]
    void CmdFire()
    {
        if (!isServer) return;
        if (pickup == null)
        {
            GameObject wave = ResourceManager.InstantiateWave(transform.position + transform.forward * 2);
            wave.transform.forward = transform.forward;
            NetworkServer.Spawn(wave);
            RpcShake();
        }
        else
        {
            CmdDropPickup(transform.position + transform.forward, transform.forward * 100 + rigid.velocity * 50);
        }
    }

    [ClientRpc]
    void RpcShake()
    {
        shaker.DoShake();
    }

    void Fire()
    {
        CmdFire();
    }

    public void SetPickup(Pickup p)
    {
        pickup = p;

        staff.gameObject.SetActive(!p);
        orb.gameObject.SetActive(p);
    }

    IEnumerator ToOrb()
    {
        Debug.Log("adasd");
        Vector3 pos = staff.transform.localPosition;
        while (staff.transform.localPosition.y > -0.1f)
        {
            pos.y = Mathf.Lerp(pos.y, -0.1f, Time.deltaTime * 2);
            staff.transform.localPosition = pos;
            yield return null;
        }
        pos.y = -0.1f;
        staff.transform.localPosition = pos;
        staff.gameObject.SetActive(false);
        pos = orb.transform.localPosition;
        pos.y = -0.3f;
        orb.transform.localPosition = pos;

        while (orb.transform.localPosition.y < 0.1f)
        {
            pos.y = Mathf.Lerp(pos.y, 0.1f, Time.deltaTime * 2);
            orb.transform.localPosition = pos;
            yield return null;
        }

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canShoot && cooldown < 0)
        {
            cooldown = defaultCooldown;
            anim.Attack();
        }
        if (cooldown > 0)
            cooldown -= Time.deltaTime;
    }
}
