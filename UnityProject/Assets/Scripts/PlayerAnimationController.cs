﻿using System;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{

    private Rigidbody rigid;
    private Animator anim;
    private float speed = 0;
    private float angle = 1;
    private int hashSpeed = Animator.StringToHash("Speed");
    private int hashAngle = Animator.StringToHash("Angle");
    private int hashAttack = Animator.StringToHash("Attack");
    [SerializeField]
    private Sprite frontStop;
    [SerializeField]
    private Sprite backStop;

    private SpriteRenderer sRenderer;
    [HideInInspector]
    public bool myself = false;

    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        sRenderer = GetComponent<SpriteRenderer>();
    }

    public void Attack()
    {
        anim.SetTrigger(hashAttack);
    }

    void Update()
    {
        if (myself) return;
        speed = rigid.velocity.magnitude;
        speed = speed < 0.1f ? 0 : 1;
        anim.SetFloat(hashSpeed, speed);
        angle = Vector3.Dot(transform.forward, (Camera.main.transform.position - transform.position).normalized);
        anim.SetFloat(hashAngle, angle);
        if (angle < 0)
        {
            sRenderer.sprite = backStop;
            sRenderer.flipX = true;
        }
        else
        {
            sRenderer.sprite = frontStop;
            sRenderer.flipX = false;
        }
    }
}
