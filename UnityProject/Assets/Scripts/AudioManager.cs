﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{

    private AudioSource source;
    [SerializeField]
    private AudioClip waveSound;

    public void PlayWaveSound()
    {
        source.PlayOneShot(waveSound);
    }
}
