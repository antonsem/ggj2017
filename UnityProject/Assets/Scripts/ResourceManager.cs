﻿using UnityEngine;
using UnityEngine.Networking;

public static class ResourceManager
{
    public static GameObject InstantiateWave(Vector3 pos)
    {
        return Object.Instantiate(Resources.Load("Prefabs/Wave"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject InstantiatePickup(Vector3 pos)
    {
        return Object.Instantiate(Resources.Load("Prefabs/Pickup"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject InstantiateRockBang(Vector3 pos)
    {
        return Object.Instantiate(Resources.Load("Prefabs/RockBang"), pos, Quaternion.identity) as GameObject;
    }
}
