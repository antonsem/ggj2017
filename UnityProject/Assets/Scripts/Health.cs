﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Health : NetworkBehaviour
{

    public const int maxHealth = 5;

    [SyncVar]
    public int currentHealth = maxHealth;

    public GameObject[] healthBar;

    public void TakeDamage(int amount)
    {
        if (!isLocalPlayer)
            return;

        currentHealth += amount;
        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
        OnChangeHealth(currentHealth);
        if (currentHealth <= 0)
            currentHealth = 0;
    }

    void OnChangeHealth(int health)
    {
        for (int i = 0; i < healthBar.Length; i++)
            healthBar[i].SetActive(i < health);
    }
}