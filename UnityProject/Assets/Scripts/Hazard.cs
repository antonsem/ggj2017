﻿using UnityEngine;

public class Hazard : MonoBehaviour
{
    public GameManager manager;

    void OnCollisionEnter(Collision col)
    {
        Player player = col.gameObject.GetComponent<Player>();
        if (player)
        {
            player.UpdateHealth(-1);
            if (!player.dead)
                player.CmdRespawn();
        }

        Pickup pickup = col.gameObject.GetComponent<Pickup>();
        if (pickup)
        {
            Destroy(pickup.gameObject);
            manager.CmdRespawnPickup();
        }
    }
}
