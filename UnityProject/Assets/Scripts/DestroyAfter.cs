﻿using UnityEngine;

public class DestroyAfter : MonoBehaviour
{

    public float timer = 1;

    void Start()
    {
        Destroy(gameObject, timer);
    }

}
