﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{

    private MeshFilter meshF;
    private float timer = 2;
    private Vector3[] defaultPos;
    private Vector3[] nextPos;

    void Start()
    {
        meshF = GetComponent<MeshFilter>();
        defaultPos = meshF.mesh.vertices;
        nextPos = new Vector3[defaultPos.Length];
    }

    void Update()
    {
        if (timer < 0)
        {
            timer = 2;
            for (int i = 0; i < defaultPos.Length; i++)
                nextPos[i] = defaultPos[i] + Vector3.up * Random.Range(-0.5f, 0.5f);
        }
        else
            timer -= Time.deltaTime;
        for (int i = 0; i < defaultPos.Length; i++)
            meshF.mesh.vertices[i] = Vector3.Lerp(meshF.mesh.vertices[i], nextPos[i], Time.deltaTime * 2);
    }
}
