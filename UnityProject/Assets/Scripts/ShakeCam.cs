﻿using UnityEngine;

public class ShakeCam : MonoBehaviour
{

    public bool Shaking;
    private float ShakeDecay;
    private float ShakeIntensity;
    private Quaternion OriginalRot;

    void Start()
    {
        Shaking = false;
    }

    void Update()
    {
        if (ShakeIntensity > 0)
        {
            //transform.position = OriginalPos + Random.insideUnitSphere * ShakeIntensity;
            transform.rotation = new Quaternion(OriginalRot.x + Random.Range(-ShakeIntensity, ShakeIntensity) * .2f,
                                      OriginalRot.y + Random.Range(-ShakeIntensity, ShakeIntensity) * .2f,
                                      OriginalRot.z + Random.Range(-ShakeIntensity, ShakeIntensity) * .2f,
                                      OriginalRot.w + Random.Range(-ShakeIntensity, ShakeIntensity) * .2f);

            ShakeIntensity -= ShakeDecay * Time.deltaTime;
        }
        else if (Shaking)
        {
            Shaking = false;
        }

    }

    public void DoShake()
    {
        OriginalRot = transform.rotation;

        ShakeIntensity = 0.3f;
        ShakeDecay = 1f;
        Shaking = true;
    }
}
