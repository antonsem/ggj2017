﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public GameManager manager;

    void OnTriggerEnter(Collider col)
    {
        Player p = col.GetComponent<Player>();
        if (p)
        {
            p.UpdateHealth(p.pickup ? 1 : 0);
            p.CmdRespawn();
        }

        if (col.GetComponent<Pickup>() != null)
        {
            Destroy(col.gameObject);
            manager.CmdRespawnPickup();
        }
    }
}
