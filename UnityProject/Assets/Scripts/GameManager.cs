﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour
{

    public Transform[] pickupPos;
    private List<Player> players = new List<Player>();

    void Start()
    {
        if (isServer)
            CmdRespawnPickup();
    }

    public Color PlayerColor(Player p)
    {
        if (!players.Contains(p))
        {
            players.Add(p);
            if (players.Count == 1)
                return Color.green;
            else if (players.Count == 2)
                return Color.red;
            else if (players.Count == 3)
                return Color.blue;
        }
        return Color.cyan;

    }

    [Command]
    public void CmdRespawnPickup()
    {
        GameObject pickup = ResourceManager.InstantiatePickup(pickupPos[Random.Range(0, pickupPos.Length)].position);
        NetworkServer.Spawn(pickup);
    }
}
